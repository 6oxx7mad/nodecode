const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const cervezaController = require('../controllers/cervezaController.js')

router.get('/', (req, res) => {
  console.log('rutas de cervezas')
  cervezaController.index(req, res)
  // let page = req.query.page ? req.query.page : 1;
  // const page = req.query.page || 1
  // res.json({ mensaje: `Lista de cervezas, página ${page}` })
})

router.get('/:id', (req, res) => {
  console.log('rutas de show cervezas')
  cervezaController.show(req, res)
})

router.post('/', (req, res) => {
  cervezaController.store(req, res)
})

router.delete('/:id', (req, res) => {
  cervezaController.destroy(req, res)
})

router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
})

module.exports = router
