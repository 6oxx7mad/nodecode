const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const productController = require('../../controllers/v2/productController')
const auth = require('../../middlewares/auth')

router.use(auth.auth)

router.get('/', (req, res) => {
  productController.index(req, res)
})

router.post('/', (req, res) => {
  productController.create(req, res)
})

router.get('/:id', (req, res) => {
  productController.show(req, res)
})

router.delete('/:id', (req, res) => {
  productController.destroy(req, res)
})

router.put('/:id', (req, res) => {
  productController.update(req, res)
})

module.exports = router
