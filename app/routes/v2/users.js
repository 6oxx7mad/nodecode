const express = require('express')
const router = express.Router()
const userController = require('../../controllers/v2/userController')

// registro de un nuevo usuario
router.get('/', userController.index)
router.post('/', userController.register)

module.exports = router
