const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const cervezaController = require('../../controllers/v2/cervezaController')

router.get('/', (req, res) => {
  console.log('rutas de cervezas')
  cervezaController.index(req, res)
})
router.post('/', (req, res) => {
  console.log('rutas de cervezas')
  cervezaController.create(req, res)
})
router.get('/search', (req, res) => {
  console.log('rutas de cervezas')
  cervezaController.search(req, res)
})
router.get('/:id', (req, res) => {
  console.log('rutas de cervezas')
  cervezaController.show(req, res)
})

module.exports = router
