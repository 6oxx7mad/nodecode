const express = require('express')
const router = express.Router()
const pedidoController = require('../../controllers/v2/pedidoController')

router.get('/', (req, res) => {
  console.log('rutas de pedidos')
  pedidoController.index(req, res)
})
router.post('/', (req, res) => {
  console.log('rutas de pedidos')
  pedidoController.create(req, res)
})
/*router.get('/search', (req, res) => {
  console.log('rutas de pedidos')
  cervezaController.search(req, res)
})
router.get('/:id', (req, res) => {
  console.log('rutas de pedidos')
  cervezaController.show(req, res)
})*/

module.exports = router