const Pedido = require('../../models/v2/Pedido')
const Producto = require('../../models/v2/Product')
const bodyParser = require('body-parser')

const index = (req, res) => {
  /*Pedido.find({}).populate({ path: 'user_id', model: 'User',
  populate: [{
      path: 'productos.product_id',
      model: Producto
  }]
}).exec(function(err, pedidos) {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de productos'
      })
    }

    return res.json(pedidos)
  })*/
  Pedido.find({}).populate('user_id').populate('productos.product_id').exec(function(err, pedidos) {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de productos'
      })
    }

    return res.json(pedidos)
  })
  /*Pedido.find({}).exec(function(err, pedidos) {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de productos'
      })
    }

    return res.json(pedidos)
  })*/
}

const show = (req, res) => {
  const id = req.params.id;
  Pedido.findById(id).populate('user_id').populate('productos.producto_id').exec(function(err, pedido){
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  el producto'
      })
    }

    return res.json(pedido)
  })
}

const create = (req, res) => {
  console.log(req.body);
  //console.log(bodyParser.json());
  const pedido = new Pedido();
  pedido.user_id = req.body.user_id
  pedido.estado = req.body.estado
  pedido.productos = req.body.productos
  pedido.save((err, pedido) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la pedido',
        error: err
      })
    }
    return res.status(201).json(pedido)
  })
}


module.exports = {
  index,
  create
}