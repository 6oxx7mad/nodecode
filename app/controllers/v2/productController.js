const Product = require('../../models/v2/Product')
const { ObjectId } = require('mongodb')

const index = (req, res) => {
  Product.find((err, products) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de productos'
      })
    }
    return res.json(products)
  })
}

const create = (req, res) => {
  const product = new Product(req.body)
  product.save((err, product) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el producto',
        error: err
      })
    }
    return res.status(201).json(product)
  })
}

const show = (req, res) => {
  const id = req.params.id
  Product.findById(id, (err, product) => {
    if (err) return res.status(500).json({ message: 'error' })
    if (!product) return res.status(404).json({ message: 'not found' })
    return res.json(product)
  })
}

const destroy = (req, res) => {
  const id = req.params.id
  Product.findByIdAndDelete(id, (err, data) => {
    if (err) return res.status(500).json({ message: 'error' })
    return res.json(data)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Product.findOne({ _id: id }, (err, product) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el product',
        error: err
      })
    }
    if (!product) {
      return res.status(404).json({
        message: 'No hemos encontrado el producto'
      })
    }

    Object.assign(product, req.body)

    product.save((err, product) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el producto'
        })
      }
      if (!product) {
        return res.status(404).json({
          message: 'No hemos encontrado el producto'
        })
      }
      return res.json(product)
    })
  })
}

module.exports = {
  index,
  create,
  show,
  destroy,
  update
}
