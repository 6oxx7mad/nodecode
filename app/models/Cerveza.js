const connection = require('../config/dbconnection')

const index = function (callback) {
  const sql = 'SELECT * FROM cervezas'
  connection.query(sql, (err, result, fields) => {
    callback(err, result, fields)
  })
}

const find = function (id, callback) {
  const sql = 'SELECT * FROM cervezas where id = ?'
  connection.query(sql, [id], (err, result, fields) => {
    callback(err, result, fields)
  })
}

const create = (cerveza, callback) => {
  const sql =
    'INSERT INTO cervezas(name, container, alcohol, price)' +
    ' VALUES(?, ?, ?, ?)'
  connection.query(
    sql,
    [cerveza.name, cerveza.container, cerveza.alcohol, cerveza.price],
    (err, result) => {
      callback(err, result)
    }
  )
}

const update = (cerveza, callback) => {
  const sql = 'UPDATE cervezas SET name=?, container=? WHERE id=?'
  connection.query(
    sql,
    [cerveza.name, cerveza.container, cerveza.id],
    (err, result) => {
      callback(err, result)
    }
  )
}

const destroy = (id, callback) => {
  const sql = 'DELETE FROM cervezas WHERE id = ?'
  connection.query(sql, [id], (err, result) => {
    callback(err, result)
  })
}

module.exports = {
  index,
  create,
  update,
  destroy,
  find
}
