const mongoose = require('mongoose')
const Schema = mongoose.Schema
// cifrado de passwords.
const bcrypt = require('bcrypt-nodejs')

const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true // lo guardará en minúsculas
  },
  name: String,
  password: { type: String, select: false },
  date: { type: Date, default: Date.now() },
  lastLogin: Date
})

UserSchema.pre('save', function (next) {
  const user = this
  if (!user.isModified('password')) return next()

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err)

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(err)

      user.password = hash
      next()
    })
  })
})

module.exports = mongoose.model('User', UserSchema)
