const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User = require('../../models/v2/User')
const Product = require('../../models/v2/Product')

const pedidoSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    /*productos: [{
        product: {product_id: Schema.Types.ObjectId, ref: 'Product'},
        cantidad: Number, 
        precio: Number  
        }],*/
    productos: [{ product_id: {type: Schema.Types.ObjectId, ref: 'Product'}, cantidad: Number, precio: Number }],
    fecha: { type: Date, default: Date.now },
    estado: String 
})

const Pedido = mongoose.model('Pedido', pedidoSchema)

module.exports = Pedido