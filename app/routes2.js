const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const routerCervezas = require('./routes/v2/cervezas.js')
const routerProducts = require('./routes/v2/products.js')
const routerUsers = require('./routes/v2/users.js')
const routerPedidos = require('./routes/v2/pedidos.js')

// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API con MongoDb!' })
})
router.use('/cervezas', routerCervezas)
router.use('/products', routerProducts)
router.use('/users', routerUsers)
router.use('/pedidos', routerPedidos)


module.exports = router
